#ifndef COMMON_H
#define COMMON_H

#include <iostream>
#include <map>
#include <string>

#include "vpl/mfxdefs.h"

#define VPL_CHECK(code) \
{ \
	vplCheck((code), __FILE__, __LINE__); \
}

#define VPL_CODE_TO_CONSTANT(code) \
{ \
	case code: \
		return #code; \
}

static std::map<mfxU32, std::string> CODEC_IDS = {
	{MFX_CODEC_AVC,     "MFX_CODEC_AVC"},
	{MFX_CODEC_HEVC,    "MFX_CODEC_HEVC"},
	{MFX_CODEC_MPEG2,   "MFX_CODEC_MPEG2"},
	{MFX_CODEC_VC1,     "MFX_CODEC_VC1"},
	{MFX_CODEC_CAPTURE, "MFX_CODEC_CAPTURE"},
	{MFX_CODEC_VP9,     "MFX_CODEC_VP9"},
	{MFX_CODEC_AV1,     "MFX_CODEC_AV1"}
};

static std::map<mfxU16, std::pair<std::string, std::string>> DEVICE_IDS = {
	{ 0x6420, {"Graphics", "Xe2"}},
	{ 0x64b0, {"Graphics", "Xe2"}},
	{ 0x64a0, {"Arc Graphics", "Xe2"}},

	{ 0x0bd5, {"Data Center GPU Max 1550", "XeHPC"}},
	{ 0x0bda, {"Data Center GPU Max 1100", "XeHPC"}},

	{ 0x7d45, {"Graphics", "Xe-LPG"}},
	{ 0x7d40, {"Graphics", "Xe-LPG"}},
	{ 0x7d41, {"Graphics", "Xe-LPG"}},
	{ 0x7d51, {"Graphics", "Xe-LPG"}},
	{ 0x7d55, {"Arc Graphics", "Xe-LPG"}},
	{ 0x7d67, {"Graphics", "Xe-LPG"}},
	{ 0x7dd1, {"Graphics", "Xe-LPG"}},
	{ 0x7dd5, {"Graphics", "Xe-LPG"}},

	{ 0x5690, {"Arc A770M Graphics", "Xe-HPG"}},
	{ 0x5691, {"Arc A730M Graphics", "Xe-HPG"}},
	{ 0x5692, {"Arc A550M Graphics", "Xe-HPG"}},
	{ 0x5693, {"Arc A370M Graphics", "Xe-HPG"}},
	{ 0x5694, {"Arc A350M Graphics", "Xe-HPG"}},
	{ 0x56a0, {"Arc A770 Graphics", "Xe-HPG"}},
	{ 0x56a1, {"Arc A750 Graphics", "Xe-HPG"}},
	{ 0x56a5, {"Arc A380 Graphics", "Xe-HPG"}},
	{ 0x56a6, {"Arc A310 Graphics", "Xe-HPG"}},
	{ 0x56b0, {"Arc Pro A30M Graphics", "Xe-HPG"}},
	{ 0x56b1, {"Arc Pro A40/A50 Graphics", "Xe-HPG"}},
	{ 0x56b2, {"Arc Pro A60M Graphics", "Xe-HPG"}},
	{ 0x56b3, {"Arc Pro A60 Graphics", "Xe-HPG"}},
	{ 0x56ba, {"Arc A380E Graphics", "Xe-HPG"}},
	{ 0x56bb, {"Arc A310E Graphics", "Xe-HPG"}},
	{ 0x56bc, {"Arc A370E Graphics", "Xe-HPG"}},
	{ 0x56bd, {"Arc A350E Graphics", "Xe-HPG"}},
	{ 0x56c0, {"Data Center GPU Flex 170", "Xe-HPG"}},
	{ 0x56c1, {"Data Center GPU Flex 140", "Xe-HPG"}},

	{ 0x4680, {"UHD Graphics 770", "Xe"} },
	{ 0x4690, {"UHD Graphics 770", "Xe"} },
	{ 0x4692, {"UHD Graphics 730", "Xe"} },
	{ 0x4693, {"UHD Graphics 710", "Xe"} },
	{ 0x46a3, {"UHD Graphics", "Xe"} },
	{ 0x46a6, {"Iris Xe Graphics", "Xe"} },
	{ 0x46a8, {"Iris Xe Graphics", "Xe"} },
	{ 0x46aa, {"Iris Xe Graphics", "Xe"} },
	{ 0x46b3, {"UHD Graphics", "Xe"} },
	{ 0x46c3, {"UHD Graphics", "Xe"} },
	{ 0x4905, {"Iris Xe MAX Graphics", "Xe"}},
	{ 0x4906, {"Iris Xe Pod", "Xe"} },
	{ 0x4907, {"GPU SG-18M", "Xe"} },
	{ 0x4908, {"Iris Xe Graphics", "Xe"} },
	{ 0x4c8b, {"UHD Graphics 730", "Xe"} },
	{ 0x4c8a, {"UHD Graphics 750", "Xe"}},
	{ 0x9a40, {"Iris Xe Graphics", "Xe"}},
	{ 0x9a49, {"Iris Xe Graphics", "Xe"}},
	{ 0x9a60, {"UHD Graphics", "Xe"}},
	{ 0x9a68, {"UHD Graphics", "Xe"}},
	{ 0x9a70, {"UHD Graphics", "Xe"}},
	{ 0x9a78, {"UHD Graphics", "Xe"}},
	{ 0x9ac0, {"UHD Graphics", "Xe"}},
	{ 0x9ac9, {"UHD Graphics", "Xe"}},
	{ 0x9ad9, {"UHD Graphics", "Xe"}},
	{ 0x9af8, {"UHD Graphics", "Xe"}},
	{ 0xa780, {"UHD Graphics 770", "Xe"}},

	{ 0x8a50, {"HD Graphics", "Gen11"}},
	{ 0x8a51, {"Iris Plus Graphics", "Gen11"}},
	{ 0x8a52, {"Iris Plus Graphics", "Gen11"}},
	{ 0x8a53, {"Iris Plus Graphics", "Gen11"}},
	{ 0x8a54, {"Iris Plus Graphics", "Gen11"}},
	{ 0x8a56, {"UHD Graphics", "Gen11"}},
	{ 0x8a57, {"HD Graphics", "Gen11"}},
	{ 0x8a58, {"UHD Graphics", "Gen11"}},
	{ 0x8a59, {"HD Graphics", "Gen11"}},
	{ 0x8a5a, {"Iris Plus Graphics", "Gen11"}},
	{ 0x8a5b, {"HD Graphics", "Gen11"}},
	{ 0x8a5c, {"Iris Plus Graphics", "Gen11"}},
	{ 0x8a5d, {"HD Graphics", "Gen11"}},
	{ 0x8a71, {"HD Graphics", "Gen11"}},

	{ 0x0a84, {"HD Graphics", "Gen9"}},
	{ 0x1902, {"HD Graphics 510", "Gen9"}},
	{ 0x1906, {"HD Graphics 510", "Gen9"}},
	{ 0x190a, {"HD Graphics", "Gen9"}},
	{ 0x190b, {"HD Graphics 510", "Gen9"}},
	{ 0x190e, {"HD Graphics", "Gen9"}},
	{ 0x1912, {"HD Graphics 530", "Gen9"}},
	{ 0x1913, {"HD Graphics", "Gen9"}},
	{ 0x1915, {"HD Graphics", "Gen9"}},
	{ 0x1916, {"HD Graphics 520", "Gen9"}},
	{ 0x1917, {"HD Graphics", "Gen9"}},
	{ 0x191a, {"HD Graphics", "Gen9"}},
	{ 0x191b, {"HD Graphics 530", "Gen9"}},
	{ 0x191d, {"HD Graphics P530", "Gen9"}},
	{ 0x191e, {"HD Graphics 515", "Gen9"}},
	{ 0x1921, {"HD Graphics 520", "Gen9"}},
	{ 0x1923, {"HD Graphics 535", "Gen9"}},
	{ 0x1926, {"Iris Graphics 540", "Gen9"}},
	{ 0x1927, {"Iris Graphics 550", "Gen9"}},
	{ 0x192a, {"HD Graphics", "Gen9"}},
	{ 0x192b, {"Iris Graphics 555", "Gen9"}},
	{ 0x192d, {"Iris Pro Graphics P555", "Gen9"}},
	{ 0x1932, {"Iris Pro Graphics 580", "Gen9"}},
	{ 0x193a, {"Iris Pro Graphics P580", "Gen9"}},
	{ 0x193b, {"Iris Pro Graphics 580", "Gen9"}},
	{ 0x193d, {"Iris Pro Graphics P580", "Gen9"}},
	{ 0x1a84, {"HD Graphics", "Gen9"}},
	{ 0x1a85, {"HD Graphics", "Gen9"}},
	{ 0x3184, {"UHD Graphics 605", "Gen9"}},
	{ 0x3185, {"UHD Graphics 600", "Gen9"}},
	{ 0x3e90, {"HD Graphics 610", "Gen9"}},
	{ 0x3e91, {"Intel UHD Graphics 630", "Gen9"}},
	{ 0x3e92, {"Intel UHD Graphics 630", "Gen9"}},
	{ 0x3e93, {"UHD Graphics 610", "Gen9"}},
	{ 0x3e94, {"UHD Graphics P630", "Gen9"}},
	{ 0x3e96, {"UHD Graphics P630", "Gen9"}},
	{ 0x3e98, {"UHD Graphics 630", "Gen9"}},
	{ 0x3e99, {"HD Graphics 610", "Gen9"}},
	{ 0x3e9a, {"UHD Graphics P630", "Gen9"}},
	{ 0x3e9b, {"Intel UHD Graphics 630", "Gen9"}},
	{ 0x3e9c, {"HD Graphics 610", "Gen9"}},
	{ 0x3ea0, {"UHD Graphics 620", "Gen9"}},
	{ 0x3ea1, {"HD Graphics 610", "Gen9"}},
	{ 0x3ea2, {"UHD Graphics", "Gen9"}},
	{ 0x3ea3, {"UHD Graphics", "Gen9"}},
	{ 0x3ea4, {"UHD Graphics", "Gen9"}},
	{ 0x3ea5, {"Iris Plus Graphics 655", "Gen9"}},
	{ 0x3ea6, {"Iris Plus Graphics 645", "Gen9"}},
	{ 0x3ea7, {"HD Graphics", "Gen9"}},
	{ 0x3ea8, {"Iris Plus Graphics 655", "Gen9"}},
	{ 0x3ea9, {"UHD Graphics 620", "Gen9"}},
	{ 0x5913, {"HD Graphics", "Gen9"}},
	{ 0x5902, {"HD Graphics 610", "Gen9"}},
	{ 0x5906, {"HD Graphics 610", "Gen9"}},
	{ 0x590b, {"HD Graphics 610", "Gen9"}},
	{ 0x5908, {"HD Graphics", "Gen9"}},
	{ 0x590a, {"HD Graphics", "Gen9"}},
	{ 0x590e, {"HD Graphics", "Gen9"}},
	{ 0x5912, {"HD Graphics 630", "Gen9"}},
	{ 0x5915, {"HD Graphics", "Gen9"}},
	{ 0x5916, {"HD Graphics 620", "Gen9"}},
	{ 0x5917, {"UHD Graphics 620", "Gen9"}},
	{ 0x591a, {"HD Graphics P630", "Gen9"}},
	{ 0x591b, {"HD Graphics 630", "Gen9"}},
	{ 0x591c, {"UHD Graphics 615", "Gen9"}},
	{ 0x591d, {"HD Graphics P630", "Gen9"}},
	{ 0x591e, {"HD Graphics 615", "Gen9"}},
	{ 0x5921, {"HD Graphics 620", "Gen9"}},
	{ 0x5923, {"HD Graphics 635", "Gen9"}},
	{ 0x5926, {"Iris Plus Graphics 640", "Gen9"}},
	{ 0x5927, {"Iris Plus Graphics 650", "Gen9"}},
	{ 0x5a84, {"HD Graphics 505", "Gen9"}},
	{ 0x5a85, {"HD Graphics 500", "Gen9"}},
	{ 0x593b, {"HD Graphics", "Gen9"}},
	{ 0x87c0, {"UHD Graphics 617", "Gen9"}},
	{ 0x87ca, {"UHD Graphics", "Gen9"}},
	{ 0x9b21, {"UHD Graphics", "Gen9"}},
	{ 0x9b41, {"UHD Graphics", "Gen9"}},
	{ 0x9ba0, {"UHD Graphics", "Gen9"}},
	{ 0x9ba2, {"UHD Graphics", "Gen9"}},
	{ 0x9ba4, {"UHD Graphics", "Gen9"}},
	{ 0x9ba5, {"HD Graphics 610", "Gen9"}},
	{ 0x9ba8, {"HD Graphics 610", "Gen9"}},
	{ 0x9baa, {"UHD Graphics", "Gen9"}},
	{ 0x9bab, {"UHD Graphics", "Gen9"}},
	{ 0x9bac, {"UHD Graphics", "Gen9"}},
	{ 0x9bc0, {"UHD Graphics", "Gen9"}},
	{ 0x9bc2, {"UHD Graphics", "Gen9"}},
	{ 0x9bc4, {"UHD Graphics", "Gen9"}},
	{ 0x9bc5, {"Intel UHD Graphics 630", "Gen9"}},
	{ 0x9bc8, {"Intel UHD Graphics 630", "Gen9"}},
	{ 0x9bca, {"UHD Graphics", "Gen9"}},
	{ 0x9bcb, {"UHD Graphics", "Gen9"}},
	{ 0x9bc6, {"UHD Graphics P630", "Gen9"}},
	{ 0x9bcc, {"UHD Graphics", "Gen9"}},
	{ 0x9be6, {"UHD Graphics P630", "Gen9"}},
	{ 0x9bf6, {"UHD Graphics P630", "Gen9"}},

	{ 0x1606, {"HD Graphics", "Gen8"} },
	{ 0x160a, {"HD Graphics", "Gen8"} },
	{ 0x160b, {"HD Graphics", "Gen8"} },
	{ 0x160d, {"HD Graphics", "Gen8"} },
	{ 0x160e, {"HD Graphics", "Gen8"} },
	{ 0x1612, {"HD Graphics 5600", "Gen8"} },
	{ 0x1616, {"HD Graphics 5500", "Gen8"} },
	{ 0x161a, {"HD Graphics P5700", "Gen8"} },
	{ 0x161b, {"HD Graphics", "Gen8"} },
	{ 0x161d, {"HD Graphics", "Gen8"} },
	{ 0x161e, {"HD Graphics 5300", "Gen8"} },
	{ 0x1622, {"Iris Pro Graphics 6200", "Gen8"} },
	{ 0x1626, {"HD Graphics 6000", "Gen8"} },
	{ 0x162a, {"Iris Pro Graphics P6300", "Gen8"} },
	{ 0x162b, {"Iris Graphics 6100", "Gen8"} },
	{ 0x162d, {"HD Graphics", "Gen8"} },
	{ 0x162e, {"HD Graphics", "Gen8"} },
	{ 0x1602, {"HD Graphics", "Gen8"} },
	{ 0x22b0, {"HD Graphics", "Gen8"} },
	{ 0x22b1, {"HD Graphics XXX", "Gen8"} },
	{ 0x22b2, {"HD Graphics", "Gen8"} },
	{ 0x22b3, {"HD Graphics", "Gen8"} },

	{ 0x0152, {"HD Graphics 2500", "Gen7"} },
	{ 0x0155, {"HD Graphics", "Gen7"} },
	{ 0x0156, {"HD Graphics 2500", "Gen7"} },
	{ 0x0157, {"HD Graphics", "Gen7"} },
	{ 0x015a, {"HD Graphics", "Gen7"} },
	{ 0x016a, {"HD Graphics P4000", "Gen7"} },
	{ 0x0162, {"HD Graphics 4000", "Gen7"} },
	{ 0x0166, {"HD Graphics 4000", "Gen7"} },
	{ 0x0402, {"HD Graphics", "Gen7"} },
	{ 0x0406, {"HD Graphics", "Gen7"} },
	{ 0x040a, {"HD Graphics", "Gen7"} },
	{ 0x040b, {"HD Graphics", "Gen7"} },
	{ 0x040e, {"HD Graphics", "Gen7"} },
	{ 0x0412, {"HD Graphics 4600", "Gen7"} },
	{ 0x0416, {"HD Graphics 4600", "Gen7"} },
	{ 0x041a, {"HD Graphics P4600/P4700", "Gen7"} },
	{ 0x041b, {"HD Graphics", "Gen7"} },
	{ 0x041e, {"HD Graphics 4400", "Gen7"} },
	{ 0x0422, {"HD Graphics", "Gen7"} },
	{ 0x0426, {"HD Graphics", "Gen7"} },
	{ 0x042a, {"HD Graphics", "Gen7"} },
	{ 0x042b, {"HD Graphics", "Gen7"} },
	{ 0x042e, {"HD Graphics", "Gen7"} },
	{ 0x0a02, {"HD Graphics", "Gen7"} },
	{ 0x0a06, {"HD Graphics", "Gen7"} },
	{ 0x0a0a, {"HD Graphics", "Gen7"} },
	{ 0x0a0b, {"HD Graphics", "Gen7"} },
	{ 0x0a0e, {"HD Graphics", "Gen7"} },
	{ 0x0a12, {"HD Graphics", "Gen7"} },
	{ 0x0a16, {"HD Graphics 4400", "Gen7"} },
	{ 0x0a1a, {"HD Graphics", "Gen7"} },
	{ 0x0a1b, {"HD Graphics", "Gen7"} },
	{ 0x0a1e, {"HD Graphics 4200", "Gen7"} },
	{ 0x0a22, {"HD Graphics", "Gen7"} },
	{ 0x0a26, {"HD Graphics 5000", "Gen7"} },
	{ 0x0a2a, {"HD Graphics", "Gen7"} },
	{ 0x0a2b, {"HD Graphics", "Gen7"} },
	{ 0x0a2e, {"Iris Graphics 5100", "Gen7"} },
	{ 0x0c02, {"HD Graphics", "Gen7"} },
	{ 0x0c06, {"HD Graphics", "Gen7"} },
	{ 0x0c0a, {"HD Graphics", "Gen7"} },
	{ 0x0c0b, {"HD Graphics", "Gen7"} },
	{ 0x0c0e, {"HD Graphics", "Gen7"} },
	{ 0x0c12, {"HD Graphics", "Gen7"} },
	{ 0x0C16, {"HD Graphics", "Gen7"} },
	{ 0x0c1a, {"HD Graphics", "Gen7"} },
	{ 0x0c1b, {"HD Graphics", "Gen7"} },
	{ 0x0c1e, {"HD Graphics", "Gen7"} },
	{ 0x0c22, {"HD Graphics", "Gen7"} },
	{ 0x0c26, {"HD Graphics", "Gen7"} },
	{ 0x0c2a, {"HD Graphics", "Gen7"} },
	{ 0x0c2b, {"HD Graphics", "Gen7"} },
	{ 0x0c2e, {"HD Graphics", "Gen7"} },
	{ 0x0d02, {"HD Graphics", "Gen7"} },
	{ 0x0d06, {"HD Graphics", "Gen7"} },
	{ 0x0d0a, {"HD Graphics", "Gen7"} },
	{ 0x0d0b, {"HD Graphics", "Gen7"} },
	{ 0x0d0e, {"HD Graphics", "Gen7"} },
	{ 0x0d12, {"HD Graphics 4600", "Gen7"} },
	{ 0x0d16, {"HD Graphics", "Gen7"} },
	{ 0x0d1a, {"HD Graphics", "Gen7"} },
	{ 0x0d1b, {"HD Graphics", "Gen7"} },
	{ 0x0d1e, {"HD Graphics", "Gen7"} },
	{ 0x0d22, {"Iris Pro Graphics 5200", "Gen7"} },
	{ 0x0d26, {"Iris Pro Graphics P5200", "Gen7"} },
	{ 0x0d2a, {"HD Graphics", "Gen7"} },
	{ 0x0d2b, {"HD Graphics", "Gen7"} },
	{ 0x0d2e, {"HD Graphics", "Gen7"} },
	{ 0x0f30, {"HD Graphics", "Gen7"} },
	{ 0x0f31, {"HD Graphics", "Gen7"} },
	{ 0x0f32, {"HD Graphics", "Gen7"} },
	{ 0x0f33, {"HD Graphics", "Gen7"} },

	{ 0x0102, {"HD Graphics 2000", "Gen6"} },
	{ 0x0106, {"HD Graphics 2000", "Gen6"} },
	{ 0x010a, {"HD Graphics 2000", "Gen6"} },
	{ 0x0112, {"HD Graphics 3000", "Gen6"} },
	{ 0x0116, {"HD Graphics 3000", "Gen6"} },
	{ 0x0122, {"HD Graphics 3000", "Gen6"} },
	{ 0x0126, {"HD Graphics 3000", "Gen6"} },

	{ 0x0042, {"HD Graphics", "Gen5"} },
	{ 0x0046, {"HD Graphics", "Gen5"} },

	{ 0x2972, {"GMA 3000", "Gen4"} },
	{ 0x2982, {"GMA X3500", "Gen4"} },
	{ 0x2992, {"GMA 3000", "Gen4"} },
	{ 0x29a2, {"GMA X3000", "Gen4"} },
	{ 0x2a02, {"GMA X3100", "Gen4"} },
	{ 0x2a12, {"GMA X3100", "Gen4"} },
	{ 0x2a42, {"GMA 4500MHD", "Gen4"} },
	{ 0x2e12, {"GMA 4500", "Gen4"} },
	{ 0x2e22, {"GMA X4500HD", "Gen4"} },
	{ 0x2e32, {"GMA X4500", "Gen4"} },
	{ 0x2e42, {"GMA 4500", "Gen4"} },
	{ 0x2e92, {"GMA 4500", "Gen4"} },

	{ 0x2582, {"GMA 900", "Gen3"} },
	{ 0x258a, {"GMA 900", "Gen3"} },
	{ 0x2592, {"GMA 900", "Gen3"} },
	{ 0x2772, {"GMA 950", "Gen3"} },
	{ 0x27a2, {"GMA 950", "Gen3"} },
	{ 0x27ae, {"GMA 950", "Gen3"} },
	{ 0x29b2, {"GMA 3100", "Gen3"} },
	{ 0x29c2, {"GMA 3100", "Gen3"} },
	{ 0x29d2, {"GMA 3100", "Gen3"} },
	{ 0xa001, {"GMA 3150", "Gen3"} },
	{ 0xa011, {"GMA 3150", "Gen3"} },

	{ 0x2562, {"Extreme Graphics", "Gen2"} },
	{ 0x2572, {"Extreme Graphics 2", "Gen2"} },
	{ 0x3577, {"Extreme Graphics", "Gen2"} },
	{ 0x3582, {"Extreme Graphics 2", "Gen2"} },
	{ 0x358E, {"Extreme Graphics 2", "Gen2"} },

	{ 0x1132, {"3D graphics with Direct AGP", "Gen1"} },
	{ 0x1240, {"752", "Gen1"} },
	{ 0x7121, {"3D graphics with Direct AGP", "Gen1"} },
	{ 0x7123, {"3D graphics with Direct AGP", "Gen1"} },
	{ 0x7125, {"3D graphics with Direct AGP", "Gen1"} },
	{ 0x7800, {"i740", "Gen1"} },
};

static std::map<mfxU32, std::string> FOURCCS = {
	{MFX_FOURCC_NV12,       "MFX_FOURCC_NV12"},
	{MFX_FOURCC_YV12,       "MFX_FOURCC_YV12"},
	{MFX_FOURCC_NV16,       "MFX_FOURCC_NV16"},
	{MFX_FOURCC_YUY2,       "MFX_FOURCC_YUY2"},
	{MFX_FOURCC_RGB565,     "MFX_FOURCC_RGB565"},
	{MFX_FOURCC_RGBP,       "MFX_FOURCC_RGBP"},
	{MFX_FOURCC_RGB3,       "MFX_FOURCC_RGB3"},
	{MFX_FOURCC_RGB4,       "MFX_FOURCC_RGB4"},
	{MFX_FOURCC_P8,         "MFX_FOURCC_P8"},
	{MFX_FOURCC_P8_TEXTURE, "MFX_FOURCC_P8_TEXTURE"},
	{MFX_FOURCC_P010,       "MFX_FOURCC_P010"},
	{MFX_FOURCC_P016,       "MFX_FOURCC_P016"},
	{MFX_FOURCC_P210,       "MFX_FOURCC_P210"},
	{MFX_FOURCC_BGR4,       "MFX_FOURCC_BGR4"},
	{MFX_FOURCC_A2RGB10,    "MFX_FOURCC_A2RGB10"},
	{MFX_FOURCC_ARGB16,     "MFX_FOURCC_ARGB16"},
	{MFX_FOURCC_ABGR16,     "MFX_FOURCC_ABGR16"},
	{MFX_FOURCC_R16,        "MFX_FOURCC_R16"},
	{MFX_FOURCC_AYUV,       "MFX_FOURCC_AYUV"},
	{MFX_FOURCC_AYUV_RGB4,  "MFX_FOURCC_AYUV_RGB4"},
	{MFX_FOURCC_UYVY,       "MFX_FOURCC_UYVY"},
	{MFX_FOURCC_Y210,       "MFX_FOURCC_Y210"},
	{MFX_FOURCC_Y410,       "MFX_FOURCC_Y410"},
	{MFX_FOURCC_Y216,       "MFX_FOURCC_Y216"},
	{MFX_FOURCC_Y416,       "MFX_FOURCC_Y416"}
};

static std::map<mfxIMPL, std::string> IMPLEMENTATIONS = {
	{MFX_IMPL_AUTO,          "MFX_IMPL_AUTO"},
	{MFX_IMPL_SOFTWARE,      "MFX_IMPL_SOFTWARE"},
	{MFX_IMPL_HARDWARE,      "MFX_IMPL_HARDWARE"},
	{MFX_IMPL_AUTO_ANY,      "MFX_IMPL_AUTO_ANY"},
	{MFX_IMPL_HARDWARE_ANY,  "MFX_IMPL_HARDWARE_ANY"},
	{MFX_IMPL_HARDWARE2,     "MFX_IMPL_HARDWARE2"},
	{MFX_IMPL_HARDWARE3,     "MFX_IMPL_HARDWARE3"},
	{MFX_IMPL_HARDWARE4,     "MFX_IMPL_HARDWARE4"},
	{MFX_IMPL_RUNTIME,       "MFX_IMPL_RUNTIME"},
	// {MFX_IMPL_SINGLE_THREAD, "MFX_IMPL_SINGLE_THREAD"},
	{MFX_IMPL_UNSUPPORTED,   "MFX_IMPL_UNSUPPORTED"}
};

static std::map<mfxU16, std::string> MEDIA_ADAPTER_TYPES = {
	{MFX_MEDIA_UNKNOWN,    "Unknown type"},
	{MFX_MEDIA_INTEGRATED, "Integrated Intel Gen Graphics adapter"},
	{MFX_MEDIA_DISCRETE,   "Discrete Intel Gen Graphics adapter"}
};

static std::map<mfxU16, std::string> PLATFORM_CODE_NAMES = {
	{MFX_PLATFORM_UNKNOWN,       "Unknown platform"},
	{MFX_PLATFORM_HASWELL,       "Haswell"},
	{MFX_PLATFORM_BROADWELL,     "Broadwell"},
	{MFX_PLATFORM_SANDYBRIDGE,   "Sandy Bridge"},
	{MFX_PLATFORM_IVYBRIDGE,     "Ivy Bridge"},
	{MFX_PLATFORM_BAYTRAIL,      "Bay Trail"},
	{MFX_PLATFORM_CHERRYTRAIL,   "Cherry Trail"},
	{MFX_PLATFORM_SKYLAKE,       "Skylake"},
	{MFX_PLATFORM_APOLLOLAKE,    "Apollo Lake"},
	{MFX_PLATFORM_KABYLAKE,      "Kaby Lake"},
	{MFX_PLATFORM_GEMINILAKE,    "Gemini Lake"},
	{MFX_PLATFORM_COFFEELAKE,    "Coffee Lake"},
	{MFX_PLATFORM_CANNONLAKE,    "Cannon Lake"},
	{MFX_PLATFORM_ICELAKE,       "Ice Lake"},
	{MFX_PLATFORM_JASPERLAKE,    "Jasper Lake"},
	{MFX_PLATFORM_ELKHARTLAKE,   "Elkhart Lake"},
	{MFX_PLATFORM_TIGERLAKE,     "Tiger Lake"},
	{MFX_PLATFORM_ROCKETLAKE,    "Rocket Lake"},
	{MFX_PLATFORM_ALDERLAKE_N,   "Alder Lake N"},
	{MFX_PLATFORM_ALDERLAKE_P,   "Alder Lake P"},
	{MFX_PLATFORM_ALDERLAKE_S,   "Alder Lake S"},
	{MFX_PLATFORM_METEORLAKE,    "Meteor Lake"},
	{MFX_PLATFORM_ARROWLAKE,     "Arrow Lake"},
	{MFX_PLATFORM_LUNARLAKE,     "Lunar Lake"},
	{MFX_PLATFORM_KEEMBAY,       "Keem Bay"},
	{MFX_PLATFORM_ARCTICSOUND_P, "Arctic Sound P"},
	{MFX_PLATFORM_XEHP_SDV,      "XeHP SDV"},
	{MFX_PLATFORM_DG2,           "Arc Alchemist/Arctic Sound M"},
	{MFX_PLATFORM_BATTLEMAGE,    "Arc Battlemage"}
};

static std::map<mfxI32, std::string> STATUS_STRINGS = {
	{MFX_ERR_NONE,                     "no error"},
	{MFX_ERR_UNKNOWN,                  "unknown error"},
	{MFX_ERR_NULL_PTR,                 "null pointer"},
	{MFX_ERR_UNSUPPORTED,              "undeveloped feature"},
	{MFX_ERR_MEMORY_ALLOC,             "failed to allocate memory"},
	{MFX_ERR_NOT_ENOUGH_BUFFER,        "insufficient buffer at input/output"},
	{MFX_ERR_INVALID_HANDLE,           "invalid handle"},
	{MFX_ERR_LOCK_MEMORY,              "failed to lock the memory block"},
	{MFX_ERR_NOT_INITIALIZED,          "member function called before initialization"},
	{MFX_ERR_NOT_FOUND,                "the specified object is not found"},
	{MFX_ERR_MORE_DATA,                "expect more data at input"},
	{MFX_ERR_MORE_SURFACE,             "expect more surface at output"},
	{MFX_ERR_DEVICE_LOST,              "lose the HW acceleration device"},
	{MFX_ERR_ABORTED,                  "operation aborted"},
	{MFX_ERR_INCOMPATIBLE_VIDEO_PARAM, "incompatible video parameters"},
	{MFX_ERR_INVALID_VIDEO_PARAM,      "invalid video parameters"},
	{MFX_ERR_UNDEFINED_BEHAVIOR,       "undefined behavior"},
	{MFX_ERR_DEVICE_FAILED,            "device operation failure"},
	{MFX_ERR_MORE_BITSTREAM,           "expect more bitstream buffers at output"},
	/*
	{MFX_ERR_INCOMPATIBLE_AUDIO_PARAM, "incompatible audio parameters"},
	{MFX_ERR_INVALID_AUDIO_PARAM,      "invalid audio parameters"},
	*/
	{MFX_ERR_REALLOC_SURFACE,          "bigger output surface required"},
	{MFX_WRN_IN_EXECUTION,             "the previous asynchronous operation is in execution"},
	{MFX_WRN_DEVICE_BUSY,              "the HW acceleration device is busy"},
	{MFX_WRN_VIDEO_PARAM_CHANGED,      "the video parameters are changed during decoding"},
	{MFX_WRN_PARTIAL_ACCELERATION,     "SW is used"},
	{MFX_WRN_INCOMPATIBLE_VIDEO_PARAM, "incompatible video parameters"},
	{MFX_WRN_VALUE_NOT_CHANGED,        "the value is saturated based on its valid range"},
	{MFX_WRN_OUT_OF_RANGE,             "the value is out of valid range"},
	{MFX_WRN_FILTER_SKIPPED,           "one of requested filters has been skipped"},
	/*
	{MFX_WRN_INCOMPATIBLE_AUDIO_PARAM, "incompatible audio parameters"},
	*/
	{MFX_ERR_NONE_PARTIAL_OUTPUT,      "frame is not ready, but bitstream contains partial output"},
	{MFX_TASK_DONE,                    "task has been completed"},
	{MFX_TASK_WORKING,                 "there is some more work to do"},
	{MFX_TASK_BUSY,                    "task is waiting for resources"},
	{MFX_ERR_MORE_DATA_SUBMIT_TASK,    "return MFX_ERR_MORE_DATA but submit internal asynchronous task"}
};

inline const std::string vplGetErrorConstant(const mfxStatus code)
{
	switch (code)
    {
		VPL_CODE_TO_CONSTANT(MFX_ERR_NONE)
		VPL_CODE_TO_CONSTANT(MFX_ERR_UNKNOWN)
		VPL_CODE_TO_CONSTANT(MFX_ERR_NULL_PTR)
		VPL_CODE_TO_CONSTANT(MFX_ERR_UNSUPPORTED)
		VPL_CODE_TO_CONSTANT(MFX_ERR_MEMORY_ALLOC)
		VPL_CODE_TO_CONSTANT(MFX_ERR_NOT_ENOUGH_BUFFER)
		VPL_CODE_TO_CONSTANT(MFX_ERR_INVALID_HANDLE)
		VPL_CODE_TO_CONSTANT(MFX_ERR_LOCK_MEMORY)
		VPL_CODE_TO_CONSTANT(MFX_ERR_NOT_INITIALIZED)
		VPL_CODE_TO_CONSTANT(MFX_ERR_NOT_FOUND)
		VPL_CODE_TO_CONSTANT(MFX_ERR_MORE_DATA)
		VPL_CODE_TO_CONSTANT(MFX_ERR_MORE_SURFACE)
		VPL_CODE_TO_CONSTANT(MFX_ERR_ABORTED)
		VPL_CODE_TO_CONSTANT(MFX_ERR_DEVICE_LOST)
		VPL_CODE_TO_CONSTANT(MFX_ERR_INCOMPATIBLE_VIDEO_PARAM)
		VPL_CODE_TO_CONSTANT(MFX_ERR_INVALID_VIDEO_PARAM)
		VPL_CODE_TO_CONSTANT(MFX_ERR_UNDEFINED_BEHAVIOR)
		VPL_CODE_TO_CONSTANT(MFX_ERR_DEVICE_FAILED)
		VPL_CODE_TO_CONSTANT(MFX_ERR_MORE_BITSTREAM)
		/*
		VPL_CODE_TO_CONSTANT(MFX_ERR_INCOMPATIBLE_AUDIO_PARAM)
		VPL_CODE_TO_CONSTANT(MFX_ERR_INVALID_AUDIO_PARAM)
		*/
		VPL_CODE_TO_CONSTANT(MFX_ERR_GPU_HANG)
		VPL_CODE_TO_CONSTANT(MFX_ERR_REALLOC_SURFACE)
		VPL_CODE_TO_CONSTANT(MFX_WRN_IN_EXECUTION)
		VPL_CODE_TO_CONSTANT(MFX_WRN_DEVICE_BUSY)
		VPL_CODE_TO_CONSTANT(MFX_WRN_VIDEO_PARAM_CHANGED)
		VPL_CODE_TO_CONSTANT(MFX_WRN_PARTIAL_ACCELERATION)
		VPL_CODE_TO_CONSTANT(MFX_WRN_INCOMPATIBLE_VIDEO_PARAM)
		VPL_CODE_TO_CONSTANT(MFX_WRN_VALUE_NOT_CHANGED)
		VPL_CODE_TO_CONSTANT(MFX_WRN_OUT_OF_RANGE)
		VPL_CODE_TO_CONSTANT(MFX_WRN_FILTER_SKIPPED)
		// VPL_CODE_TO_CONSTANT(MFX_WRN_INCOMPATIBLE_AUDIO_PARAM)
		VPL_CODE_TO_CONSTANT(MFX_ERR_NONE_PARTIAL_OUTPUT)
		// VPL_CODE_TO_CONSTANT(MFX_TASK_DONE)
		VPL_CODE_TO_CONSTANT(MFX_TASK_WORKING)
		VPL_CODE_TO_CONSTANT(MFX_TASK_BUSY)
		VPL_CODE_TO_CONSTANT(MFX_ERR_MORE_DATA_SUBMIT_TASK)
		default:
			return "UNKNOWN_VPL_STATUS";
    }
}

inline bool vplCheck(const mfxStatus code, const std::string file, const int line, const bool abort = false)
{
	if (code != MFX_ERR_NONE)
	{
		std::cerr << "----- !!! The following oneVPL status occurred !!! -----" << std::endl;
		std::cerr << "code:     " << code << std::endl;
		std::cerr << "contant:  " << vplGetErrorConstant(code) << std::endl;
		std::cerr << "string:   " << STATUS_STRINGS[code] << std::endl;
		std::cerr << "location: " << file << ":" << line << std::endl;
		std::cerr << "--------------------------------------------------------" << std::endl;

		if(abort)
			exit(code);

		return false;
	}

	return (code == MFX_ERR_NONE);
}

#endif
