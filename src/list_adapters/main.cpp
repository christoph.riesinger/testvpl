#include <cassert>
#include <cstring>
#include <iostream>

#include "vpl/mfx.h"

#include "common.h"

int
mfxImpl2dxgiIdx(mfxIMPL mfxImpl)
{
	if (MFX_IMPL_BASETYPE(mfxImpl) == MFX_IMPL_HARDWARE) //default adapter
		return 0;
	else
		return MFX_IMPL_BASETYPE(mfxImpl) - MFX_IMPL_HARDWARE2 + 1;
}

int
dxgiIdx2mfxImpl(int dxgiIdx)
{
	if (dxgiIdx == 0)
		return MFX_IMPL_HARDWARE;
	else
		return MFX_IMPL_HARDWARE2 + dxgiIdx - 1;
}

int
main()
{
	mfxU32 numOfAdapters;
	mfxAdapterInfo* adapters;
	mfxAdaptersInfo adaptersInfo;
	mfxU32 adapterNumber;
	mfxU16 adapterCodeName;
	mfxU16 adapterDeviceId;
	mfxU16 adapterMediaType;
	mfxIMPL adapterImplementation;
	mfxLoader loader;
	mfxSession session;
	mfxVersion version;

	std::cout << "Deployed with VPL API version: " << MFX_VERSION_MAJOR << "." << MFX_VERSION_MINOR  << std::endl;
	VPL_CHECK(MFXQueryAdaptersNumber(&numOfAdapters))
	std::cout << "Number of adapters:            " << numOfAdapters << std::endl << std::endl;

	if (numOfAdapters)
	{
		adapters = new mfxAdapterInfo[numOfAdapters];
		memset(adapters, 0, numOfAdapters * sizeof(mfxAdapterInfo));
		adaptersInfo = mfxAdaptersInfo{ adapters, numOfAdapters, 0 };
		VPL_CHECK(MFXQueryAdapters(nullptr, &adaptersInfo))
		assert(adaptersInfo.NumActual == numOfAdapters);
	}

	for (mfxU32 i = 0; i < numOfAdapters; i++)
	{
		std::cout << "Adapter " << (i + 1) << "/" << numOfAdapters << ":" << std::endl;

		adapterNumber =         adapters[i].Number;
		adapterCodeName =       adapters[i].Platform.CodeName;
		adapterDeviceId =       adapters[i].Platform.DeviceId;
		adapterMediaType =      adapters[i].Platform.MediaAdapterType;
		adapterImplementation = dxgiIdx2mfxImpl(adapterNumber);

		std::cout << " Number            " << adapterNumber << std::endl;
		std::cout << " CodeName          " << adapterCodeName;
		if (PLATFORM_CODE_NAMES.count(adapterCodeName))
			std::cout << " (" << PLATFORM_CODE_NAMES[adapterCodeName] << ")";
		std::cout << std::endl;
		std::cout << " DeviceId          0x" << std::hex << adapterDeviceId << std::dec;
		if (DEVICE_IDS.count(adapterDeviceId))
			std::cout << " (" << DEVICE_IDS[adapterDeviceId].first << " (" << DEVICE_IDS[adapterDeviceId].second << "))";
		std::cout << std::endl;
		std::cout << " MediaAdapterType  " << adapterMediaType;
		if (MEDIA_ADAPTER_TYPES.count(adapterMediaType))
			std::cout << " (" << MEDIA_ADAPTER_TYPES[adapterMediaType] << ")";
		std::cout << std::endl;
		std::cout << " Implementation    " << adapterImplementation;
		if (IMPLEMENTATIONS.count(adapterImplementation))
			std::cout << " (" << IMPLEMENTATIONS[adapterImplementation] << ")";
		std::cout << std::endl;

		// todo: add error handling
		loader = MFXLoad();

		VPL_CHECK(MFXCreateSession(loader, 0, &session));

		VPL_CHECK(MFXQueryVersion(session, &version))
		std::cout << " API version       " << version.Major << "." << version.Minor << std::endl;

		// todo: add error handling
		MFXUnload(loader);
	}

	delete[] adapters;
	
	return 0;
}
